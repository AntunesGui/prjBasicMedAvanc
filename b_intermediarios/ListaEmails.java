package b_intermediarios;
import java.util.List;
import java.nio.file.Paths;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

/**
 * Crie um programa que lê os clientes presentes no arquivo contas.csv e 
 * filtre aqueles que possuem o saldo superior à 7000.
 * Desses clientes, criar outro arquivo de texto que possua todos os clientes
 * no seguinte formato:
 * 
 * João da Silva<joaosilva@teste.com>, Maria da Penha<maria@teste.com>
 * 
 * 		ArrayList<String> lista = new ArrayList<>();

		lista.add("Uma Palavra");
		lista.get(0);
		lista.size();
		lista.remove(0);	
 */
/**Primeira opcao de FOR abaixo
 * 			for(int i = 0; i < lista.size(); i++) {
	System.out.println(lista.get(i));
}
 */
public class ListaEmails {
	public static void main(String[] args) {
		Path path = Paths.get("contas.csv");
		Path outputPath = Paths.get("/home/mastertech/emails.txt");
		StringBuilder emails = new StringBuilder();
		System.out.println("Informe sue ID: ");
		Scanner scanner = new Scanner(System.in);
		int id = scanner.nextInt();

		try {
			List<String> lista = Files.readAllLines(path);
			/**removendo a primeira linha do arquivo pois eh string
			 */
			lista.remove(0);

			for(String linha : lista) {
				String[] partes = linha.split(",");

				int saldo = Integer.parseInt(partes[4]);
				if(saldo > 7000) {
					emails.append(partes[1] + " " + partes[2] + "<" + partes[3] + ">, ");
					//					System.out.println(saldo);
				}
			}
			Files.write(outputPath, emails.toString().getBytes());
			//			System.out.println(emails.toString());

		}catch (IOException e) {

			e.printStackTrace();
		}

	}

}

