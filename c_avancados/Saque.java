package c_avancados;
import java.util.List;
import java.util.Scanner;
import java.nio.file.Paths;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;


/**
 * Crie um programa que faça um saque no arquivo contas.csv. O programa
 * deve receber um id e o valor do saque. Ele deve informar caso o id
 * não seja encontrado ou caso o valor não esteja disponível.
 * 
 * Após o saque, o arquivo deve ser atualizado.
 * 
 */
public class Saque {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int id, valor;
		System.out.println("Informe seu ID: ");
		id = scanner.nextInt();
		List<String> lista = lerContas();

		if(lista == null) {
			System.out.println("Não foi possível ler o arquivo");
			return;
		}

		boolean achouCliente = false;

		for (int i = 0; i < lista.size(); i++) {
			String[] partes = lista.get(i).split(",");
			int idlista;
			int valorlista;
			try {
				idlista = Integer.parseInt(partes[0]);
				if(id == idlista) {
					achouCliente = true;

					System.out.println("Informe o valor: ");
					valor = scanner.nextInt();
					valorlista = Integer.parseInt(partes[4]);
					if(valor > valorlista) {
						System.out.println("Saldo insuficiente");
					}else {
						int saldoFinal = valorlista - valor;
						partes[4] = Integer.toString(saldoFinal);
						String linha = "";
						for (int j = 0; j < partes.length; j++) {
							if(j == 4) {
								linha += partes[j];
							}else {
								linha += partes[j] + ",";
							}
							
						}
						lista.set(i, linha);
					}
					break;
				}
			} catch (Exception e) {
				// TODO: handle exception
			}
		}



		if(!achouCliente) {
			System.out.println("ID inexistente!");
		}


//		StringBuilder emails = new StringBuilder();
//
//		for(String linha: lista) {
//			String[] partes = linha.split(",");
//
//			int saldo = Integer.parseInt(partes[4]);
//
//			if(saldo > 7000) {
//				String email = String.format("%s %s<%s>, ", partes[1], partes[2], partes[3]);
//				emails.append(email);
//			}
//		}

		gravarArquivo(lista);

	}

	public static List<String> lerContas(){
		Path path = Paths.get("contas.csv");

		List<String> lista;

		try {
			lista = Files.readAllLines(path);
		} catch (IOException e) {
			return null;
		}


		return lista;
	}

	public static void gravarArquivo(List<String> emails) {
		Path outputPath = Paths.get("/home/mastertech/emails.txt");

		try {
			Files.write(outputPath,emails);
		} catch (IOException e) {
			System.out.println("Não foi possível gravar o arquivo");
		}
	}
}



